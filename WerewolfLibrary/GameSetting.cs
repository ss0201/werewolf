﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace WerewolfLibrary
{
    [DataContract]
    public class GameSetting
    {
        [DataMember]
        public Dictionary<TimeConfigurablePhases, int> Times { get; set; }
        [DataMember]
        public int VoteLimit { get; set; }
        [DataMember]
        public ClockModes DayClockMode { get; set; }
        [DataMember]
        public Dictionary<Player.Roles, int> RoleCounts { get; set; }
        [DataMember]
        public Dictionary<Player.Roles, string> RoleNames { get; set; }

        public GameSetting()
        {
            Times = new Dictionary<TimeConfigurablePhases, int>();
            foreach (TimeConfigurablePhases phase in Enum.GetValues(typeof(TimeConfigurablePhases)))
            {
                Times.Add(phase, 1);
            }

            RoleCounts = new Dictionary<Player.Roles, int>();
            foreach (Player.Roles role in Enum.GetValues(typeof(Player.Roles)))
            {
                RoleCounts.Add(role, 0);
            }
        }
    }
}
