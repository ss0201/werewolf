﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.IO;

namespace WerewolfLibrary
{
    public class AsyncState
    {
        public static int PrefixLength = (int)Math.Log(ushort.MaxValue + 1, byte.MaxValue + 1);

        public Socket Socket { get; private set; }
        public byte[] DataBuffer { get; private set; }
        public MemoryStream DataStream { get; private set; }
        public int RemainingDataLength { get; private set; }

        public AsyncState(Socket socket)
        {
            Socket = socket;
            DataBuffer = new byte[1024];
            DataStream = new MemoryStream();
            RemainingDataLength = 0;
        }

        public void BeginReceiveingData()
        {
            RemainingDataLength = BitConverter.ToUInt16(DataBuffer, 0);
        }

        public void WriteReceivedData(int length)
        {
            DataStream.Write(DataBuffer, 0, length);
            RemainingDataLength -= length;
        }

        public void RestartReception()
        {
            DataStream.Close();
            DataStream = new MemoryStream();
        }
    }
}
