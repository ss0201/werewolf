﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace WerewolfLibrary
{
    public class ConnectionClosedException : ApplicationException
    {
        public ConnectionClosedException()
        {
        }

        public ConnectionClosedException(string message)
            : base(message)
        {
        }

        protected ConnectionClosedException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public ConnectionClosedException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
