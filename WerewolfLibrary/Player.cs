﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace WerewolfLibrary
{
    public class Player : INotifyPropertyChanged
    {
        public enum Roles
        {
            Villager,
            Werewolf,
            Priest,
            Necromancer,
            Fanatic,
            Knight,
            Fairy,
            Vampire,
        }

        public enum Statuses
        {
            Alive,
            Dead,
        }

        public enum Colors
        {
            White,
            Black,
        }

        public enum Camps
        {
            Villager,
            Werewolf,
            Vampire,
        }

        public string Name { get; private set; }
        public Roles Role { get; private set; }
        public Statuses Status { get { return status; } private set { status = value; OnPropertyChanged("Status"); } }
        private Statuses status = Statuses.Alive;
        public KillMethods KilledMethod { get { return killedMethod; } private set { killedMethod = value; OnPropertyChanged("KilledMethod"); } }
        private KillMethods killedMethod;
        public int KilledTurn { get { return killedTurn; } private set { killedTurn = value; OnPropertyChanged("KilledTurn"); } }
        private int killedTurn;
        public bool IsSacrifice { get; private set; }

        public Player(string name, Roles role = Roles.Villager, bool isSacrifice = false)
        {
            Name = name;
            Role = role;
            IsSacrifice = isSacrifice;
        }

        public void Die(KillMethods method, int turn)
        {
            Status = Statuses.Dead;
            KilledMethod = method;
            KilledTurn = turn;
        }

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
