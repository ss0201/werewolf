﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace WerewolfLibrary
{
    [DataContract]
    public class GeneralSetting
    {
        [DataMember]
        public string PlayerImageDirectoryName { get; set; }
        [DataMember]
        public string RoleImageDirectoryName { get; set; }
    }
}
