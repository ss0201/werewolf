﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace WerewolfLibrary.Report
{
    [DataContract]
    public class StatementReport : CommandReport
    {
        public enum Types
        {
            Normal,
            Monologue,
            Graveyard,
        }
        public const int MaxStatementLength = 1000;

        [DataMember]
        public string Statement
        {
            get { return statement; }
            set { statement = new string(value.Take(MaxStatementLength).ToArray()); }
        }
        private string statement;
        [DataMember]
        public bool IsStrong { get; set; }
        [DataMember]
        public Types Type { get; set; }
    }
}
