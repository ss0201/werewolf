﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace WerewolfLibrary.Report
{
    [DataContract]
    public class EndVoteReport : Report
    {
        [DataMember]
        public string KilledName { get; set; }
        [DataMember]
        public Dictionary<string, string> Votes { get; set; } // <Subject, Target>

        public override string ToSystemMessage()
        {
            string message = string.Empty;

            if (string.IsNullOrEmpty(KilledName))
            {
                message += "Revote!";
            }
            else
            {
                message += "Vote is over!";
            }

            foreach (KeyValuePair<string, string> vote in Votes)
            {
                message += Environment.NewLine + vote.Key + "\t->\t" + vote.Value;
            }

            if (!string.IsNullOrEmpty(KilledName))
            {
                message += Environment.NewLine + "KILLED : " + KilledName;
            }


            return message;
        }
    }
}
