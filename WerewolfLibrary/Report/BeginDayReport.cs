﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace WerewolfLibrary.Report
{
    [DataContract]
    public class BeginDayReport : Report
    {
        [DataMember]
        public List<string> KilledNames { get; set; }
        [DataMember]
        public string TargetName { get; set; }
        [DataMember]
        public Nullable<Player.Colors> AbilityResult { get; set; }

        public override string ToSystemMessage()
        {
            string message = "Day has broken...";

            foreach (string killedName in KilledNames)
            {
                message += Environment.NewLine + "KILLED : " + killedName;
            }

            if (!string.IsNullOrEmpty(TargetName))
            {
                if (AbilityResult == null)
                {
                    message += Environment.NewLine + "ABILITY : You used your ability to " + TargetName;
                }
                else
                {
                    message += Environment.NewLine + "ABILITY : " + TargetName + " was " + AbilityResult.ToString();
                }
            }

            return message;
        }
    }
}
