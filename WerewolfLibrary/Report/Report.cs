﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace WerewolfLibrary.Report
{
    [DataContract]
    [KnownType(typeof(BeginDayReport))]
    [KnownType(typeof(BeginGameReport))]
    [KnownType(typeof(BeginNightReport))]
    [KnownType(typeof(BeginVoteReport))]
    [KnownType(typeof(CommandReport))]
    [KnownType(typeof(EndVoteReport))]
    [KnownType(typeof(EndGameReport))]
    [KnownType(typeof(ResetGameReport))]
    [KnownType(typeof(RoleSelectionReport))]
    [KnownType(typeof(SpoilerReport))]
    [KnownType(typeof(StatementReport))]
    public abstract class Report
    {
        public virtual string ToSystemMessage()
        {
            return string.Empty;
        }
    }
}
