﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace WerewolfLibrary.Report
{
    [DataContract]
    public class BeginVoteReport : Report
    {
        public override string ToSystemMessage()
        {
            return "Time to Vote!";
        }
    }
}
