﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Collections.ObjectModel;

namespace WerewolfLibrary.Report
{
    [DataContract]
    public class RoleSelectionReport : CommandReport
    {
        [DataMember]
        public List<RoleBoolPair> Wishes { get; set; }
    }
}
