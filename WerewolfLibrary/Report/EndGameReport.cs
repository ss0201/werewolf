﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System;

namespace WerewolfLibrary.Report
{
    [DataContract]
    public class EndGameReport : Report
    {
        [DataMember]
        public Player.Camps Winner { get; set; }
        [DataMember]
        public Dictionary<string, string> Roles { get; set; }
        [DataMember]
        public Dictionary<string, string> UserNames { get; set; }

        public override string ToSystemMessage()
        {
            string message = "Game is over!" + Environment.NewLine +
                "The winner is : " + Winner.ToString() + "!";

            foreach (string playerName in UserNames.Keys)
            {
                message += Environment.NewLine + playerName + "\t:\t" + Roles[playerName] + 
                    "\t:\t" + UserNames[playerName];
            }

            return message;
        }
    }
}
