﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace WerewolfLibrary.Report
{
    [DataContract]
    public class AbilityReport : CommandReport
    {
        [DataMember]
        public string TargetName { get; set; }
    }
}
