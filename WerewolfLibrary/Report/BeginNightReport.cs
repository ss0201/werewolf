﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace WerewolfLibrary.Report
{
    [DataContract]
    public class BeginNightReport : Report
    {
        public override string ToSystemMessage()
        {
            return "Night has come...";
        }
    }
}
