﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using WerewolfLibrary;

namespace WerewolfLibrary.Report
{
    [DataContract]
    public class ClientInformationReport : Report
    {
        [DataMember]
        public List<ClientInformation> ClientInformations { get; set; }
    }
}
