﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace WerewolfLibrary.Report
{
    [DataContract]
    [KnownType(typeof(AbilityReport))]
    [KnownType(typeof(ClientInformationReport))]
    [KnownType(typeof(StatementReport))]
    public class CommandReport : Report
    {
        [DataMember]
        public string SubjectName { get; set; }
        [DataMember]
        public Phases Phase { get; set; }
        [DataMember]
        public int Turn { get; set; }
    }
}
