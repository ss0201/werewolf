﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace WerewolfLibrary.Report
{
    [DataContract]
    public class SpoilerReport : Report
    {
        [DataMember]
        public Dictionary<string, string> Roles { get; set; }

        public override string ToSystemMessage()
        {
            string message = "Your are Dead!" + Environment.NewLine + "Spoiler :";

            foreach (string playerName in Roles.Keys)
            {
                message += Environment.NewLine + playerName + "\t:\t" + Roles[playerName];
            }

            return message;
        }
    }
}
