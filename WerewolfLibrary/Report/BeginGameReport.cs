﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace WerewolfLibrary.Report
{
    [DataContract]
    public class BeginGameReport : Report
    {
        [DataMember]
        public GameSetting GameSetting { get; set; }
        [DataMember]
        public string Variant { get; set; }
        [DataMember]
        public List<string> PlayerNames { get; set; }
        [DataMember]
        public string YourName { get; set; }
        [DataMember]
        public Player.Roles YourRole { get; set; }
        [DataMember]
        public List<string> Allies { get; set; }
        [DataMember]
        public string SacrificeName { get; set; }

        public override string ToSystemMessage()
        {
            string message = "Now the game has begun!" + Environment.NewLine;

            foreach (TimeConfigurablePhases phase in Enum.GetValues(typeof(TimeConfigurablePhases)))
            {
                message += phase.ToString() + " : " + GameSetting.Times[phase] + " seconds" + Environment.NewLine;
            }
            message += "Vote Limit : " + GameSetting.VoteLimit + " times" + Environment.NewLine;

            foreach (Player.Roles role in Enum.GetValues(typeof(Player.Roles)))
            {
                message += GameSetting.RoleNames[role] + " - " + GameSetting.RoleCounts[role] + Environment.NewLine;
            }

            message += "Your Allies : ";

            foreach (string ally in Allies)
            {
                message += ally + ", ";
            }

            return message;
        }
    }
}
