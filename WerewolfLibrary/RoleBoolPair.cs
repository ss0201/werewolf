﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace WerewolfLibrary
{
    [DataContract]
    public class RoleBoolPair
    {
        [DataMember]
        public Player.Roles Key { get; set; }
        [DataMember]
        public bool Value { get; set; }

        public RoleBoolPair(Player.Roles key, bool value)
        {
            Key = key;
            Value = value;
        }
    }
}
