﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization;
using System.Windows;
using System.Windows.Media;
using System.Diagnostics;
using System.Windows.Controls;

namespace WerewolfLibrary
{
    public static class Utility
    {
        private static Random random = new Random();

        public static T RandomPop<T>(List<T> list)
        {
            return RandomPop(list, arg => true);
        }

        /// <summary>
        /// returns anything in the list if there is no element which matches predicate
        /// </summary>
        public static T RandomPop<T>(List<T> list, Func<T, bool> predicate)
        {
            T element = RandomGet(list, predicate);
            list.Remove(element);
            return element;
        }

        public static T RandomGet<T>(List<T> list)
        {
            return RandomGet(list, arg => true);
        }

        /// <summary>
        /// returns anything in the list if there is no element which matches predicate
        /// </summary>
        public static T RandomGet<T>(List<T> list, Func<T, bool> predicate)
        {
            if (list.Count == 0)
            {
                return default(T);
            }
            var validList = list.FindAll(new Predicate<T>(predicate));
            if (validList.Count == 0)
            {
                validList = list;
            }
            return validList[random.Next(validList.Count)];
        }

        public static void Shuffle<T>(List<T> list)
        {
            var tmpList = new List<T>();
            int count = list.Count;
            for (int i = 0; i < count; i++)
            {
                tmpList.Add(Utility.RandomPop(list));
            }
            list.AddRange(tmpList);
        }

        public static byte[] Serialize<T>(T target)
        {
            var serializer = new DataContractSerializer(typeof(T));
            MemoryStream stream = new MemoryStream();
            serializer.WriteObject(stream, target);
            return stream.ToArray();
        }

        public static T Deserialize<T>(byte[] target)
        {
            return Deserialize<T>(new MemoryStream(target));
        }

        public static T Deserialize<T>(Stream stream)
        {
            var serializer = new DataContractSerializer(typeof(T));
            return (T)serializer.ReadObject(stream);
        }

        public static T DeepCopy<T>(T target)
        {
            return Deserialize<T>(Serialize(target));
        }

        public static childItem FindVisualChild<childItem>(DependencyObject obj)
        where childItem : DependencyObject
        {
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(obj, i);
                if (child != null && child is childItem)
                    return (childItem)child;
                else
                {
                    childItem childOfChild = FindVisualChild<childItem>(child);
                    if (childOfChild != null)
                        return childOfChild;
                }
            }
            return null;
        }

        public static void WriteToEventLog(string text)
        {
            string source = "Werewolf";

            try
            {
                if (!EventLog.SourceExists(source))
                {
                    EventLog.CreateEventSource(source, "Application");
                }
                EventLog.WriteEntry(source, text);
            }
            catch { }
        }

        public static void ShowStatus(string text, Application current)
        {
            if (current != null)
            {
                current.Dispatcher.BeginInvoke(new Action(() =>
                {
                    if (current.MainWindow != null)
                    {
                        ((TextBlock)current.MainWindow.FindName("StatusTextBlock")).Text = text;
                    }
                }));
            }
        }

        public static List<T> GetModes<T>(IEnumerable<T> enumerable)
        {
            if (enumerable == null)
            {
                return new List<T>();
            }

            var frequencies = new Dictionary<T, int>();
            foreach (T element in enumerable)
            {
                if (frequencies.ContainsKey(element))
                {
                    frequencies[element]++;
                }
                else
                {
                    frequencies.Add(element, 1);
                }
            }
            return (from f in frequencies where f.Value == frequencies.Values.Max() select f.Key).ToList();
        }

        public static T ReadObjectFromFile<T>(string file)
        {
            try
            {
                using (var fileStream = new FileStream(file, FileMode.Open, FileAccess.Read))
                {
                    return Utility.Deserialize<T>(fileStream);
                }
            }
            catch
            {
                return default(T);
            }
        }

        public static void WriteObjectToFile<T>(string file, T obj)
        {
            try
            {
                using (var fileStream = new FileStream(file, FileMode.Create, FileAccess.Write))
                {
                    byte[] serialized = Utility.Serialize<T>(obj);
                    fileStream.Write(serialized, 0, serialized.Length);
                }
            }
            catch { }
        }
    }
}
