﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WerewolfLibrary
{
    public enum Phases
    {
        Preparation,
        Day,
        Vote,
        Night,
        Gameover,
    }

    public enum TimeConfigurablePhases
    {
        Day,
        Vote,
        Revote,
        Night,
    }
}
