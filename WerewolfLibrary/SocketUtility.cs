﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.IO;

namespace WerewolfLibrary
{
    public static class SocketUtility
    {
        public static void SendReport(Socket socket, Report.Report report)
        {
            if (socket == null || !socket.Connected)
            {
                return;
            }

            try
            {
                byte[] serialized = Utility.Serialize(report);
                byte[] prefix = BitConverter.GetBytes((ushort)serialized.Length);

                socket.Send(prefix.Concat(serialized).ToArray());
            }
            catch (Exception e)
            {
                Utility.WriteToEventLog(e.ToString());
            }
        }

        public static void ReceiveDataCallback(IAsyncResult asyncResult, Village village,
            AsyncCallback callback, Action<string> showStatus)
        {
            AsyncState asyncState = (AsyncState)asyncResult.AsyncState;

            if (!asyncState.Socket.Connected)
            {
                return;
            }

            int length = 0;
            try
            {
                length = asyncState.Socket.EndReceive(asyncResult);
            }
            catch (System.ObjectDisposedException)
            {
                throw new ConnectionClosedException();
            }
            catch
            {
                showStatus("Error while receiving data");
                return;
            }

            // disconnected?
            if (length <= 0)
            {
                asyncState.Socket.Close();
                throw new ConnectionClosedException();
            }

            try
            {
                if (asyncState.RemainingDataLength == 0)
                {
                    asyncState.BeginReceiveingData();
                }
                else
                {
                    asyncState.WriteReceivedData(length);
                }
            }
            catch
            {
                showStatus("Error while writing received data");
                return;
            }

            int lengthOfDataToReceive;

            // received all?
            if (asyncState.RemainingDataLength == 0)
            {
                try
                {
                    village.ProcessCommand(asyncState.DataStream, asyncState.Socket);
                }
                catch (Exception e)
                {
                    showStatus("ProcessCommand failed");
                    Utility.WriteToEventLog(e.ToString());
                    return;
                }
                finally
                {
                    asyncState.RestartReception();
                    lengthOfDataToReceive = AsyncState.PrefixLength;
                }
            }
            else
            {
                lengthOfDataToReceive = Math.Min(asyncState.RemainingDataLength, asyncState.DataBuffer.Length);
            }

            // restart
            try
            {
                asyncState.Socket.BeginReceive(asyncState.DataBuffer, 0, lengthOfDataToReceive,
                    SocketFlags.None, new AsyncCallback(callback), asyncState);
            }
            catch
            {
                showStatus("BeginReceive failed");
                return;
            }
        }
    }
}
