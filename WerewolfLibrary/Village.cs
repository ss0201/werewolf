﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using System.Net;
using System.IO;
using System.Xml;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Net.Sockets;

namespace WerewolfLibrary
{
    public abstract class Village : INotifyPropertyChanged
    {
        public GameSetting GameSetting { get { return gameSetting; } set { gameSetting = value; OnPropertyChanged("GameSetting"); } }
        public Phases Phase { get { return phase; } private set { phase = value; OnPropertyChanged("Phase"); } }
        public int Turn { get { return turn; } set { turn = value; OnPropertyChanged("Turn"); } }
        public int VoteCount { get { return voteCount; } private set { voteCount = value; OnPropertyChanged("VoteCount"); } }
        public TimeSpan RemainingTime { get { return remainingTime; } set { remainingTime = value; OnPropertyChanged("RemainingTime"); } }
        private GameSetting gameSetting;
        private Phases phase = Phases.Preparation;
        private int turn;
        private int voteCount;
        private TimeSpan remainingTime = TimeSpan.Zero;
        private Timer clock = new Timer();

        public Village()
        {
            clock.Interval = 1000;
            clock.Elapsed += new ElapsedEventHandler((sender, e) =>
            {
                if (RemainingTime.TotalSeconds >= 0)
                {
                    RemainingTime -= new TimeSpan(0, 0, 1);
                }
                else
                {
                    RemainingTime = TimeSpan.Zero;
                    clock.Stop();
                }
            });
            Turn = 0;
        }

        public void ProcessCommand(MemoryStream stream, Socket socket)
        {
            stream.Seek(0, SeekOrigin.Begin);
            var report = Utility.Deserialize<Report.Report>(stream);

            Execute(report, socket);
        }

        protected void BeginPhase(Phases phase)
        {
            Phase = phase;
            switch (phase)
            {
                case Phases.Day:
                    Turn++;
                    VoteCount = 0;
                    break;
                case Phases.Gameover:
                    Turn++;
                    RemainingTime = TimeSpan.Zero;
                    break;
                case Phases.Vote:
                    VoteCount++;
                    break;
            }
        }

        protected abstract void Execute(Report.Report report, Socket socket);

        protected void StartClock(int second)
        {
            RemainingTime = new TimeSpan(0, 0, second); 
            clock.Start();
        }

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
