﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace WerewolfLibrary
{
    [ValueConversion(typeof(TimeSpan), typeof(string))]
    public class ShortTimeFormatConverter : IValueConverter
    {
        public object Convert(object value, System.Type targetType,
            object parameter, System.Globalization.CultureInfo culture)
        {
            string format;
            if (parameter == null)
            {
                format = @"m\:ss";
            }
            else if ((string)parameter == "Rough")
            {
                format = @"m\:\?\?";
            }
            else
            {
                throw new InvalidOperationException();
            }

            return ((TimeSpan)value).ToString(format);
        }

        public object ConvertBack(object value, System.Type targetType,
            object parameter, System.Globalization.CultureInfo culture)
        {
            throw new InvalidOperationException();
        }
    }
}
