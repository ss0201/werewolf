﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WerewolfLibrary
{
    public static class Constant
    {
        public const string PlayerImageDirectory = "Player";
        public const string DefaultVariantName = "Default";
        public const string AnonymousName = "Anonymous";

        public static bool DefaultReadyStatus =
#if DEBUG
 true;
#else
 false;
#endif
    }
}
