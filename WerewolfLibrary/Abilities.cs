﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WerewolfLibrary
{
    public enum Abilities
    {
        Vote,
        Kill,
        See,
        Guard,
    }
}
