﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel;

namespace WerewolfLibrary
{
    [DataContract]
    public class ClientInformation : INotifyPropertyChanged
    {
        public string Name { get { return name; } set { name = value; OnPropertyChanged("Name"); } }
        [DataMember]
        private string name = string.Empty;
        public bool IsReady { get { return isReady; } set { isReady = value; OnPropertyChanged("IsReady"); } }
        [DataMember]
        private bool isReady = Constant.DefaultReadyStatus;

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
