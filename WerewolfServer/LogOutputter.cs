﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using WerewolfLibrary;
using WerewolfLibrary.Report;
using System.Net;

namespace WerewolfServer
{
    class LogOutputter : IDisposable
    {
        private const string StrongClass = "strong";
        private const string NotStrongClass = "notstrong";
        private const string BorderClass = "border";
        private const string SpeechClass = "speech";
        private const string TooltipClass = "tooltip";
        private const string ShowHideCheckboxID = "Checkbox";

        private DateTime gameBegunTime = DateTime.Now;
        private StreamWriter writer;
        private bool isDisposed = false;
        private GameSetting gameSetting;

        public LogOutputter()
        {
            try
            {
                var stream = File.Open(App.LogDirectory + @"\" + gameBegunTime.ToString("MM-dd-yyyy HH-mm-ss") + ".html",
                    FileMode.CreateNew, FileAccess.Write, FileShare.Read);
                writer = new StreamWriter(stream);
                writer.AutoFlush = true;

                writer.WriteLine("<html><head>");
                writer.WriteLine("<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>");
                writer.WriteLine("<style type='text/css'><!--");
                OutputCss();
                writer.WriteLine("--></style>");
                writer.WriteLine("<script src='http://code.jquery.com/jquery-1.5.2.min.js'></script>");
                writer.WriteLine("<script src='http://cdn.jquerytools.org/1.2.5/tiny/jquery.tools.min.js'></script>");

                writer.WriteLine("<script type='text/javascript'>");
                writer.WriteLine("$(document).ready(function() {");
                foreach (StatementReport.Types type in Enum.GetValues(typeof(StatementReport.Types)))
                {
                    writer.WriteLine("$('#{0}{1}').click(function() {{" +
                        "$('.{0}').toggle();" +
                        "}});", type, ShowHideCheckboxID);
                }
                writer.WriteLine("});");
                writer.WriteLine("</script>");

                writer.WriteLine("<title>{0}</title></head><body>", gameBegunTime.ToString("G"));

                writer.WriteLine("<p>Show:");
                foreach (StatementReport.Types type in Enum.GetValues(typeof(StatementReport.Types)))
                {
                    writer.WriteLine("<span><input type='checkbox' id='{0}{1}' checked />{0}</span>", type, ShowHideCheckboxID);
                }
                writer.WriteLine("</p>");

                writer.WriteLine("<div>");
                writer.WriteLine("<h1>{0}</h1>", gameBegunTime.ToString("G"));
            }
            catch { }
        }

        ~LogOutputter()
        {
            Dispose();
        }

        private void OutputCss()
        {
            writer.WriteLine(".{0} {{ background-color: lightgreen; }}", StatementReport.Types.Monologue);
            writer.WriteLine(".{0} {{ background-color: lightgrey; }}", StatementReport.Types.Graveyard);
            writer.WriteLine(".{0} {{ background-color: lightyellow; }}", Phases.Day);
            writer.WriteLine(".{0} {{ background-color: pink; }}", Phases.Vote);
            writer.WriteLine(".{0} {{ background-color: lightblue; }}", Phases.Night);
            writer.WriteLine(".{0} {{ font-weight: bold; }}", StrongClass);
            writer.WriteLine(".{0} {{ font-weight: normal; }}", NotStrongClass);

            string[] tags = { "p", "h1", "h2" };
            foreach (string tag in tags)
            {
                writer.WriteLine("{0} {{ text-align: center; }}", tag);
            }
            writer.WriteLine("table { margin-left: auto; margin-right: auto; margin-top: 0.5em; margin-bottom: 0.5em; }");
            writer.WriteLine("table td { padding-left: 0.5em; padding-right: 0.5em; }");
            writer.WriteLine("table.{0} {{ border-collapse: collapse; border: 1px solid; }}", BorderClass);
            writer.WriteLine("table.{0} td {{ border: 1px solid; }}", BorderClass);
            writer.WriteLine("table.{0} {{ border-style: double; }}", SpeechClass);
            writer.WriteLine("table.{0} td {{ border: 1px solid; max-width: 40em }}", SpeechClass);
            writer.WriteLine(".{0} {{ display: none; background-color: white; border: 1px solid; padding: 0.5em; }}",
                TooltipClass);
        }

        public void Dispose()
        {
            if (!isDisposed)
            {
                try
                {
                    writer.WriteLine("</table>");
                    writer.WriteLine("</body></html>");
                    writer.Close();
                }
                catch { }
                isDisposed = true;
            }
        }

        public void BeginGame(GameSetting gameSetting, IEnumerable<ConcretePlayer> players)
        {
            this.gameSetting = gameSetting;
            try
            {
                writer.WriteLine("<h2>Game Informations</h2>");

                writer.WriteLine("<table class=border><caption>General Settings</caption>");
                foreach (TimeConfigurablePhases phase in Enum.GetValues(typeof(TimeConfigurablePhases)))
                {
                    writer.WriteLine("<tr><td>{0} Time</td><td>{1}</td></tr>", phase.ToString(), gameSetting.Times[phase]);
                }
                writer.WriteLine("<tr><td>VoteLimit</td><td>{0}</td></tr>", gameSetting.VoteLimit);
                writer.WriteLine("</table>");

                writer.WriteLine("<table class=border><caption>Key Numbers</caption>");
                foreach (Player.Roles role in Enum.GetValues(typeof(Player.Roles)))
                {
                    writer.WriteLine("<tr><td>{0}</td><td>{1}</td></tr>",
                        WebUtility.HtmlEncode(gameSetting.RoleNames[role]), gameSetting.RoleCounts[role]);
                }
                writer.WriteLine("</table>");

                writer.WriteLine("<table class=border><caption>Players</caption>");
                foreach (ConcretePlayer player in players)
                {
                    writer.WriteLine("<tr><td>{0}</td><td>{1}</td><td>{2}</td></tr>",
                        WebUtility.HtmlEncode(player.Name), gameSetting.RoleNames[player.Role], WebUtility.HtmlEncode(player.Participant.Name));
                }
                writer.WriteLine("</table>");

                // tooltip
                writer.WriteLine("<script type='text/javascript'>");
                writer.WriteLine("$(document).ready(function() {");
                writer.WriteLine("var arg = { offset: [-5, 0], delay: 0 }");
                foreach (ConcretePlayer player in players)
                {
                    writer.WriteLine("var newarg = $.extend({{ tip: '#{0:X8}' }}, arg);", player.Name.GetHashCode());
                    writer.WriteLine("$('span:contains(\"{0}\")').tooltip(newarg);", WebUtility.HtmlEncode(player.Name));
                }
                writer.WriteLine("});");
                writer.WriteLine("</script>");

                foreach (ConcretePlayer player in players)
                {
                    writer.WriteLine("<div id='{0:X8}' class='{1}'>", player.Name.GetHashCode(), TooltipClass);
                    writer.WriteLine("{0}<br>{1}<br>{2}", WebUtility.HtmlEncode(player.Name),
                        gameSetting.RoleNames[player.Role], WebUtility.HtmlEncode(player.Participant.Name));
                    writer.WriteLine("</div>");
                }
                // -----

                writer.WriteLine("<table class={0}>", SpeechClass);
            }
            catch { }
        }

        public void BeginDay(IEnumerable<string> killedNames, int turn, IEnumerable<ConcretePlayer> players)
        {
            try
            {
                writer.WriteLine("</table>");
                writer.WriteLine("</div>");

                writer.WriteLine("<div class={0}>", Phases.Day);
                writer.WriteLine("<h2>Day {0}</h2>", turn);

                writer.WriteLine("<table><caption>Players Killed</caption>");
                foreach (string name in killedNames)
                {
                    writer.WriteLine("<tr><td>{0}</td></tr>", ModifyPlayerName(name));
                }
                writer.WriteLine("</table>");

                writer.WriteLine("<table><caption>Ability Targets</caption>");
                foreach (ConcretePlayer player in players)
                {
                    if (player.AbilityTarget != null)
                    {
                        writer.WriteLine("<tr><td>{0}</td><td>({1})</td><td>-&gt;</td><td>{2}</td></tr>",
                            ModifyPlayerName(player.Name), gameSetting.RoleNames[player.Role],
                            ModifyPlayerName(player.AbilityTarget.Name));
                    }
                }
                writer.WriteLine("</table>");

                writer.WriteLine("<table class={0}>", SpeechClass);
            }
            catch { }
        }

        public void BeginVote()
        {
            try
            {
                writer.WriteLine("</table>");
                writer.WriteLine("</div>");

                writer.WriteLine("<div class={0}>", Phases.Vote);
                writer.WriteLine("<h2>Begin Vote</h2>");

                writer.WriteLine("<table class={0}>", SpeechClass);
            }
            catch { }
        }

        public void EndVote(IDictionary<string, string> votes, string killedName)
        {
            try
            {
                writer.WriteLine("</table>");

                writer.WriteLine("<table><caption>Vote Result</caption>");
                foreach (string subject in votes.Keys)
                {
                    writer.WriteLine("<tr><td>{0}</td><td>-&gt;</td><td>{1}</td></tr>",
                        ModifyPlayerName(subject), ModifyPlayerName(votes[subject]));
                }
                writer.WriteLine("</table>");

                if (string.IsNullOrEmpty(killedName))
                {
                    writer.WriteLine("<p>Revote!</p>");
                }
                else
                {
                    writer.WriteLine("<p>{0} has been lynched!</p>", ModifyPlayerName(killedName));
                }

                writer.WriteLine("<table class={0}>", SpeechClass);
            }
            catch { }
        }

        public void BeginNight(int turn)
        {
            try
            {
                writer.WriteLine("</table>");
                writer.WriteLine("</div>");

                writer.WriteLine("<div class={0}>", Phases.Night);
                writer.WriteLine("<h2>Night {0}</h2>", turn);

                writer.WriteLine("<table class={0}>", SpeechClass);
            }
            catch { }
        }

        public void EndGame(Player.Camps winner)
        {
            try
            {
                writer.WriteLine("</table>");
                writer.WriteLine("</div>");

                writer.WriteLine("<div class={0}>", Phases.Gameover);
                writer.WriteLine("<h2>Conclusion</h2>");
                writer.WriteLine("<p>The winner is {0}!</p>", winner);

                writer.WriteLine("<table class={0}>", SpeechClass);
            }
            catch { }
        }

        public void Speak(StatementReport report)
        {
            try
            {
                writer.Write("<tr class='{0}'>", report.Type.ToString().ToLower());
                writer.Write("<td>{0}</td>", ModifyPlayerName(report.SubjectName));
                writer.Write("<td class='{0}'>", report.IsStrong ? StrongClass : NotStrongClass);
                writer.WriteLine("{0}</td></tr>", WebUtility.HtmlEncode(report.Statement).Replace(Environment.NewLine, "<br />"));
            }
            catch { }
        }

        private string ModifyPlayerName(string name)
        {
            return "<span>" + WebUtility.HtmlEncode(name) + "</span>";
        }
    }
}
