﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WerewolfLibrary;
using WerewolfLibrary.Report;

namespace WerewolfServer
{
    public class ConcretePlayer : Player
    {
        public ConcretePlayer AbilityTarget { get; set; }
        public Participant Participant { get; private set; }

        public ConcretePlayer(string name, Roles role, bool isSacrifice, Participant participant)
            : base(name, role, isSacrifice)
        {
            Participant = participant;
        }

        public void SendReport(Report report)
        {
            if (Participant != null)
            {
                Participant.SendReport(report);
            }
        }
    }
}
