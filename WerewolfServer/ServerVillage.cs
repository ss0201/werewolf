﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using System.Net;
using WerewolfLibrary;
using WerewolfLibrary.Report;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Net.Sockets;

namespace WerewolfServer
{
    public class ServerVillage : Village, IDisposable
    {
        private List<ConcretePlayer> players = new List<ConcretePlayer>();
        private LogOutputter logOutputter;
        private bool isDisposed = false;
        private Timer timer;

        public ServerVillage()
            : base()
        {
            GameSetting = Utility.ReadObjectFromFile<GameSetting>(App.SettingDirectory + @"\" + App.GameSettingFile);
            if (GameSetting == null)
            {
                GameSetting = new GameSetting();
            }
        }

        public ServerVillage(IEnumerable<Client> clients)
            : this()
        {
            foreach (Client client in clients)
            {
                client.SendReport(new ResetGameReport());
            }
            SendClientInformations();
        }

        ~ServerVillage()
        {
            Dispose();
        }

        public void Dispose()
        {
            if (!isDisposed)
            {
                try
                {
                    if (!Directory.Exists(App.SettingDirectory))
                    {
                        Directory.CreateDirectory(App.SettingDirectory);
                    }
                    GameSetting.RoleCounts[Player.Roles.Villager] = 0;
                    Utility.WriteObjectToFile<GameSetting>(App.SettingDirectory + @"\" + App.GameSettingFile, GameSetting);

                    if (timer != null)
                    {
                        timer.Close();
                    }
                    if (logOutputter != null)
                    {
                        logOutputter.Dispose();
                    }

                    isDisposed = true;
                }
                catch { }
            }
        }

        public void BeginGame()
        {
            switch (Phase)
            {
                case Phases.Day:
                case Phases.Vote:
                case Phases.Night:
                    return;
            }

            var variantSetting = new VariantSetting();
            Turn = 1;

            var tmpParticipants = new List<Participant>(App.Clients);

#if DEBUG
            for (int i = 0; i < 8; i++)
            {
                tmpParticipants.Add(new Bot(a => true));
            }
#endif

            var participants = new List<Participant>();
            var sacrifice = new Bot(role => role != Player.Roles.Werewolf && role != Player.Roles.Vampire);
            participants.Add(sacrifice);

            GameSetting.RoleCounts[Player.Roles.Villager] = Math.Max(0, participants.Count + tmpParticipants.Count -
                (from c in GameSetting.RoleCounts where c.Key != Player.Roles.Villager select c.Value).Sum());

            Utility.Shuffle(tmpParticipants);
            tmpParticipants.Sort(Participant.CompareByRoleCondition);
            participants.AddRange(tmpParticipants);

            var playerNamesInPool = new List<string>(variantSetting.PlayerNames);
            GameSetting.RoleNames = variantSetting.RoleNames;

            var roles = new List<Player.Roles>();
            foreach (Player.Roles role in Enum.GetValues(typeof(Player.Roles)))
            {
                for (int i = 0; i < GameSetting.RoleCounts[role]; i++)
                {
                    roles.Add(role);
                }
            }

            foreach (Participant participant in participants)
            {
                string playerName = Utility.RandomPop(playerNamesInPool);
                Player.Roles role = Utility.RandomPop(roles, participant.RoleCondition);

                var player = new ConcretePlayer(playerName, role, participant.Equals(sacrifice), participant);
                players.Add(player);
            }
            Utility.Shuffle(players);

            var playerNames = (from p in players select p.Name).ToList();

            foreach (ConcretePlayer player in players)
            {
                var allies = new List<string>();
                switch (player.Role)
                {
                    case Player.Roles.Werewolf:
                    case Player.Roles.Fairy:
                        allies = (
                            from p in players
                            where p.Role == player.Role && !p.Equals(player)
                            select p.Name
                            ).ToList();
                        break;
                }
                player.SendReport(new BeginGameReport()
                {
                    GameSetting = GameSetting,
                    Variant = variantSetting.VariantName,
                    PlayerNames = playerNames,
                    YourName = player.Name,
                    YourRole = player.Role,
                    Allies = allies,
                    SacrificeName = players.Find(p => p.Participant.Equals(sacrifice)).Name,
                });
            }

            logOutputter = new LogOutputter();
            logOutputter.BeginGame(GameSetting, players);

            BeginNight();
        }

        public void BeginDay(object sender, ElapsedEventArgs e)
        {
            BeginPhase(Phases.Day);

            foreach (ConcretePlayer player in players)
            {
                if (player.AbilityTarget == null && player.Status == Player.Statuses.Alive)
                {
                    switch (player.Role)
                    {
                        case Player.Roles.Priest:
                            player.AbilityTarget = Utility.RandomGet(players, p => !p.Equals(player) && p.Status == Player.Statuses.Alive && !p.IsSacrifice);
                            break;
                        case Player.Roles.Knight:
                            if (Turn >= 3)
                            {
                                player.AbilityTarget = Utility.RandomGet(players, p => !p.Equals(player) && p.Status == Player.Statuses.Alive);
                            }
                            break;
                    }
                }
            }

            IEnumerable<ConcretePlayer> eatenPlayers;
            if (Turn == 2)
            {
                eatenPlayers = from p in players where p.IsSacrifice select p;
            }
            else
            {
                eatenPlayers = Utility.GetModes(from p in players
                                                where p.Role == Player.Roles.Werewolf &&
                                                p.AbilityTarget != null &&
                                                p.AbilityTarget.Role != Player.Roles.Werewolf
                                                select p.AbilityTarget);
                if (eatenPlayers.Count() == 0)
                {
                    eatenPlayers = players.Except(players.FindAll(p =>
                        p.Role == Player.Roles.Werewolf || p.Status == Player.Statuses.Dead));
                }
            }
            ConcretePlayer eatenPlayer = Utility.RandomGet(eatenPlayers.ToList());

            var cursedPlayers = from p in players
                                where p.Role == Player.Roles.Priest &&
                                p.AbilityTarget != null &&
                                ((ConcretePlayer)p).AbilityTarget.Role == Player.Roles.Vampire
                                select p.AbilityTarget;

            var killedPlayers = new List<ConcretePlayer>(cursedPlayers);
            if (eatenPlayer != null &&
                eatenPlayer.Role != Player.Roles.Vampire &&
                (Turn == 2 ||
                players.FindAll(subject => subject.Role == Player.Roles.Knight &&
                    subject.Status == Player.Statuses.Alive &&
                    subject.AbilityTarget == eatenPlayer).Count == 0))
            {
                killedPlayers.Add(eatenPlayer);
            }

            foreach (ConcretePlayer player in killedPlayers)
            {
                player.Die(KillMethods.EatOrCurse, Turn - 1);
            }

            var killedNames = (from p in killedPlayers.Distinct() select p.Name).ToList();

            foreach (ConcretePlayer player in players)
            {
                Nullable<Player.Colors> abilityResult = null;
                string targetName = string.Empty;
                switch (player.Role)
                {
                    case Player.Roles.Priest:
                        if (player.AbilityTarget != null)
                        {
                            abilityResult = player.AbilityTarget.Role == Player.Roles.Werewolf ?
                                Player.Colors.Black : Player.Colors.White;
                            targetName = player.AbilityTarget.Name;
                        }
                        break;
                    case Player.Roles.Necromancer:
                        var lynchedPlayer = players.Find(p =>
                            p.KilledMethod == KillMethods.Lynch && p.KilledTurn == Turn - 1);
                        if (lynchedPlayer != null)
                        {
                            abilityResult = lynchedPlayer.Role == Player.Roles.Werewolf ?
                                Player.Colors.Black : Player.Colors.White;
                            targetName = lynchedPlayer.Name;
                        }
                        break;
                    case Player.Roles.Werewolf:
                        targetName = eatenPlayer.Name;
                        break;
                    case Player.Roles.Knight:
                        if (player.AbilityTarget != null)
                        {
                            targetName = player.AbilityTarget.Name;
                        }
                        break;
                    default:
                        break;
                }

                player.SendReport(new BeginDayReport()
                {
                    KilledNames = killedNames,
                    TargetName = targetName,
                    AbilityResult = abilityResult,
                });

                if (killedNames.Contains(player.Name))
                {
                    SendSpoiler(player);
                }
            }

            logOutputter.BeginDay(killedNames, Turn, players);

            if (!IsGameover())
            {
                StartTimer();
            }
        }

        public void BeginVote(object sender, ElapsedEventArgs e)
        {
            BeginPhase(Phases.Vote);

            foreach (ConcretePlayer player in players)
            {
                player.AbilityTarget = null;
                player.SendReport(new BeginVoteReport());
            }

            logOutputter.BeginVote();

            StartTimer();
        }

        public void EndVote(object sender, ElapsedEventArgs e)
        {
            var alivePlayers = (from p in players where p.Status == Player.Statuses.Alive select p).ToList();
            foreach (ConcretePlayer player in (from p in alivePlayers
                                               where p.AbilityTarget == null
                                               select p))
            {
                player.AbilityTarget = Utility.RandomGet((from p in alivePlayers where !p.Equals(player) select p).ToList());
            }

            var lynchedPlayers = Utility.GetModes(from p in players where p.Status == Player.Statuses.Alive select p.AbilityTarget);

            string lynchedPlayerName;
            if (lynchedPlayers.Count > 1 && VoteCount < GameSetting.VoteLimit)
            {
                lynchedPlayerName = string.Empty;
            }
            else
            {
                ConcretePlayer lynchedPlayer = Utility.RandomGet(lynchedPlayers);
                lynchedPlayer.Die(KillMethods.Lynch, Turn);
                lynchedPlayerName = lynchedPlayer.Name;
            }

            var votes = new Dictionary<string, string>();
            foreach (ConcretePlayer player in alivePlayers)
            {
                votes.Add(player.Name, player.AbilityTarget.Name);
            }

            foreach (ConcretePlayer player in players)
            {
                player.SendReport(new EndVoteReport()
                {
                    KilledName = lynchedPlayerName,
                    Votes = votes,
                });
                if (player.Name == lynchedPlayerName)
                {
                    SendSpoiler(player);
                }
            }

            logOutputter.EndVote(votes, lynchedPlayerName);

            if (lynchedPlayerName == string.Empty)
            {
                BeginVote(null, null);
                return;
            }

            if (!IsGameover())
            {
                BeginNight();
            }
        }

        public void BeginNight()
        {
            BeginPhase(Phases.Night);

            foreach (ConcretePlayer player in players)
            {
                player.AbilityTarget = null;
                player.SendReport(new BeginNightReport());
            }

            logOutputter.BeginNight(Turn);

            StartTimer();
        }

        public bool IsGameover()
        {
            Player.Camps winner;
            var alivePlayers = players.FindAll(player => player.Status == Player.Statuses.Alive);
            int numWerewolves = alivePlayers.FindAll(player => player.Role == Player.Roles.Werewolf).Count;
            int numVampires = alivePlayers.FindAll(player => player.Role == Player.Roles.Vampire).Count;
            int numHumans = alivePlayers.Count - numWerewolves - numVampires;

            if (numHumans > numWerewolves)
            {
                if (numWerewolves > 0)
                {
                    return false;
                }
                else if (numVampires > 0)
                {
                    winner = Player.Camps.Vampire;
                }
                else
                {
                    winner = Player.Camps.Villager;
                }
            }
            else
            {
                if (numVampires > 0)
                {
                    winner = Player.Camps.Vampire;
                }
                else
                {
                    winner = Player.Camps.Werewolf;
                }
            }

            base.BeginPhase(Phases.Gameover);

            var roles = new Dictionary<string, string>();
            var userNames = new Dictionary<string, string>();
            foreach (ConcretePlayer player in players)
            {
                roles.Add(player.Name, GameSetting.RoleNames[player.Role]);
                userNames.Add(player.Name, player.Participant.Name);
            }

            foreach (ConcretePlayer player in players)
            {
                player.SendReport(new EndGameReport()
                {
                    Winner = winner,
                    Roles = roles,
                    UserNames = userNames,
                });
            }

            logOutputter.EndGame(winner);
            return true;
        }

        private new void BeginPhase(Phases phase)
        {
            base.BeginPhase(phase);
        }

        private void StartTimer()
        {
            Action<object, ElapsedEventArgs> callback;
            switch (Phase)
            {
                case Phases.Day:
                    callback = BeginVote; break;
                case Phases.Vote:
                    callback = EndVote; break;
                case Phases.Night:
                    callback = BeginDay; break;
                default:
                    throw new InvalidOperationException();
            }

            TimeConfigurablePhases phase;
            if (Phase == Phases.Vote && VoteCount > 1)
            {
                phase = TimeConfigurablePhases.Revote;
            }
            else
            {
                phase = (TimeConfigurablePhases)Enum.Parse(typeof(TimeConfigurablePhases), Phase.ToString());
            }
            timer = new Timer();
            timer.AutoReset = false;
            timer.Interval = GameSetting.Times[phase] * 1000;
            timer.Elapsed += new ElapsedEventHandler(callback);
            timer.Start();
            StartClock(GameSetting.Times[phase]);
        }

        private void SendSpoiler(ConcretePlayer targetPlayer)
        {
            var roles = new Dictionary<string, string>();
            foreach (ConcretePlayer player in players)
            {
                roles.Add(player.Name, GameSetting.RoleNames[player.Role]);
            }
            targetPlayer.SendReport(new SpoilerReport()
            {
                Roles = roles,
            });
        }

        protected override void Execute(Report report, Socket socket)
        {
            Execute((dynamic)report, socket);
        }

        private void Execute(ClientInformationReport report, Socket socket)
        {
            var client = App.Clients.First(c => c.AsyncState.Socket.Equals(socket));
            App.Current.Dispatcher.BeginInvoke(new Action(() =>
            {
                var clientInformation = report.ClientInformations[0];
                client.Name = clientInformation.Name;
                client.IsReady = clientInformation.IsReady;
                SendClientInformations();
            }));
        }

        private void Execute(CommandReport report, Socket socket)
        {
            if (report.Phase != Phase || report.Turn != Turn)
            {
                return;
            }

            if (Phase == Phases.Preparation)
            {
                var subjectClient = App.Clients.FirstOrDefault(c => c.Name == report.SubjectName);
                if (subjectClient != null && subjectClient.AsyncState.Socket.Equals(socket))
                {
                    Execute((dynamic)report);
                }
            }
            else
            {
                var player = players.Find(p =>
                    p.Participant is Client &&
                    ((Client)p.Participant).AsyncState.Socket.Equals(socket));

                if (player != null && report.SubjectName == player.Name)
                {
                    Execute((dynamic)report);
                }
            }
        }

        private void Execute(StatementReport report)
        {
            IEnumerable<ConcretePlayer> targetPlayers;
            switch (Phase)
            {
                case Phases.Preparation:
                    foreach (Client client in App.Clients)
                    {
                        client.SendReport(report);
                    }
                    return;
                case Phases.Gameover:
                    targetPlayers = players;
                    break;
                default:
                    var subjectPlayer = players.Find((player) => player.Name == report.SubjectName);

                    switch (report.Type)
                    {
                        case StatementReport.Types.Monologue:
                            targetPlayers = new ConcretePlayer[] { subjectPlayer };
                            break;
                        case StatementReport.Types.Graveyard:
                            if (subjectPlayer.Status == Player.Statuses.Alive) { return; }
                            targetPlayers = players.FindAll(p => p.Status == Player.Statuses.Dead);
                            break;
                        default:
                            if (subjectPlayer.Status == Player.Statuses.Dead) { return; }
                            switch (Phase)
                            {
                                case Phases.Day:
                                    targetPlayers = players;
                                    break;
                                case Phases.Night:
                                    if (subjectPlayer.Role == Player.Roles.Werewolf)
                                    {
                                        targetPlayers = players.FindAll(p => p.Role == Player.Roles.Werewolf && p.Status == Player.Statuses.Alive);
                                    }
                                    else
                                    {
                                        return;
                                    }
                                    break;
                                default:
                                    return;
                            }
                            break;
                    }
                    break;
            }

            foreach (ConcretePlayer player in targetPlayers)
            {
                player.SendReport(report);
            }

            if (logOutputter != null)
            {
                logOutputter.Speak(report);
            }
        }

        private void Execute(AbilityReport report)
        {
            ConcretePlayer subject = players.Find(player => player.Name == report.SubjectName);
            ConcretePlayer target = players.Find(player => player.Name == report.TargetName);
            subject.AbilityTarget = target;
        }

        private void Execute(RoleSelectionReport report)
        {
            App.Clients.First(p => p.Name == report.SubjectName).RoleCondition =
                role => report.Wishes.First(w => w.Key == role).Value;
        }

        public void SendClientInformations()
        {
            var clientInformations = (from c in App.Clients
                                      where !string.IsNullOrEmpty(c.Name)
                                      select new ClientInformation
                                      {
                                          Name = c.Name,
                                          IsReady = c.IsReady
                                      }).ToList();

            foreach (Client client in App.Clients)
            {
                client.SendReport(new ClientInformationReport()
                {
                    ClientInformations = clientInformations,
                });
            }
        }
    }
}
