﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using WerewolfLibrary;
using Microsoft.Win32;
using System.Windows.Data;
using System.IO;

namespace WerewolfServer
{
    /// <summary>
    /// SettingWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class SettingWindow : Window
    {
        private GameSetting tmpGameSetting;

        public SettingWindow()
        {
            InitializeComponent();
            tmpGameSetting = Utility.DeepCopy<GameSetting>(App.Village.GameSetting);

            Resources.Add("gameSetting", tmpGameSetting);
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            App.Village.GameSetting = tmpGameSetting;

            string directory = VariantTextBox.Text;
            switch (directory.Substring(directory.Length - 1))
            {
                case @"\":
                case @"/":
                    directory = directory.Remove(directory.Length - 1);
                    break;
            }
            Properties.Settings.Default.VariantDirectory = directory;

            this.Close();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
