﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using System.Collections.ObjectModel;

namespace WerewolfServer
{
    /// <summary>
    /// App.xaml の相互作用ロジック
    /// </summary>
    public partial class App : Application
    {
        public const string SettingDirectory = "Setting";
        public const string GameSettingFile = "GameSetting.xml";

        public const string RolesTag = "Roles";

        public const string VariantFile = "Variant.xml";

        public const string LogDirectory = "Log";


        public static App CurrentApp { get { return (App)(App.Current); } }

        public static ServerVillage Village { get; set; }
        public static ObservableCollection<Client> Clients { get; private set; }
        public static Communicator Communicator { get; private set; }

        private void Application_Exit(object sender, ExitEventArgs e)
        {
            WerewolfServer.Properties.Settings.Default.Save();
        }

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            Clients = new ObservableCollection<Client>();
            Communicator = new Communicator();
        }
    }
}
