﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;
using WerewolfLibrary;

namespace WerewolfServer
{
    public class Communicator
    {
        public void StartAccept(int port)
        {
            try
            {
                Socket serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                IPEndPoint endPoint = new IPEndPoint(IPAddress.Any, port);
                serverSocket.Bind(endPoint);
                serverSocket.Listen(100);
                serverSocket.BeginAccept(new AsyncCallback(AcceptCallback), serverSocket);
            }
            catch
            {
                Utility.ShowStatus("Failed to StartAccept", App.Current);
                return;
            }
        }

        private void AcceptCallback(IAsyncResult asyncResult)
        {
            Socket serverSocket = (Socket)asyncResult.AsyncState;

            Socket clientSocket = null;
            try
            {
                clientSocket = serverSocket.EndAccept(asyncResult);
            }
            catch
            {
                Utility.ShowStatus("Failed to EndAccept", App.Current);
                return;
            }

            AsyncState asyncState = new AsyncState(clientSocket);
            Client client = new Client(clientSocket, asyncState);

            App.Current.Dispatcher.BeginInvoke(new Action(() => App.Clients.Add(client)));

            try
            {
                clientSocket.BeginReceive(asyncState.DataBuffer, 0, AsyncState.PrefixLength,
                    SocketFlags.None, new AsyncCallback(ReceiveDataCallback), asyncState);
            }
            catch
            {
                Utility.ShowStatus("Failed to BeginReceive", App.Current);
                return;
            }

            Utility.ShowStatus("A client has successfully connected", App.Current);

            // restart
            serverSocket.BeginAccept(new AsyncCallback(AcceptCallback), serverSocket);
        }

        private void ReceiveDataCallback(IAsyncResult asyncResult)
        {
            try
            {
                SocketUtility.ReceiveDataCallback(asyncResult, App.Village,
                    ReceiveDataCallback, text => Utility.ShowStatus(text, App.Current));
            }
            catch (ConnectionClosedException)
            {
                Utility.ShowStatus("Connection has been closed", App.Current);

                AsyncState asyncState = (AsyncState)asyncResult.AsyncState;
                var client = App.Clients.First(c => c.AsyncState.Socket.Equals(asyncState.Socket));

                App.Current.Dispatcher.BeginInvoke(new Action(() => CloseConnection(client)));
            }
        }

        public void CloseConnections()
        {
            foreach (Client client in App.Clients)
            {
                client.CloseConnection();
            }
            App.Clients.Clear();
        }

        public void CloseConnection(Client client)
        {
            client.CloseConnection();
            App.Clients.Remove(client);
            App.Village.SendClientInformations();
        }
    }
}
