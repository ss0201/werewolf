﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;
using WerewolfLibrary;
using WerewolfLibrary.Report;

namespace WerewolfServer
{
    public class Client : Participant, IDisposable
    {
        private Socket socket;
        public AsyncState AsyncState { get; set; }
        public string IP { get; private set; }
        public bool IsReady { get { return isReady; } set { isReady = value; OnPropertyChanged("IsReady"); } }
        private bool isReady = Constant.DefaultReadyStatus;

        public Client(Socket socket, AsyncState asyncState)
            : base()
        {
            this.socket = socket;
            AsyncState = asyncState;
            IP = ((IPEndPoint)socket.RemoteEndPoint).Address.ToString();
            RoleCondition = arg => true;
        }

        public override void SendReport(Report report)
        {
            SocketUtility.SendReport(socket, report);
        }

        public void CloseConnection()
        {
            if (socket.Connected)
            {
                try
                {
                    socket.Shutdown(SocketShutdown.Both);
                    socket.Close();
                    AsyncState.Socket.Shutdown(SocketShutdown.Both);
                    AsyncState.Socket.Close();
                }
                catch { }
            }
        }

        public void Dispose()
        {
            CloseConnection();
        }
    }
}
