﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WerewolfLibrary;
using WerewolfLibrary.Report;
using System.ComponentModel;

namespace WerewolfServer
{
    public abstract class Participant : INotifyPropertyChanged
    {
        public Func<Player.Roles, bool> RoleCondition { get; set; }
        public string Name { get { return name; } set { name = value; OnPropertyChanged("Name"); } }
        private string name = string.Empty;


        public abstract void SendReport(Report report);

        public static int CompareByRoleCondition(Participant left, Participant right)
        {
            int leftValidRoles = 0, rightValidRoles = 0;

            foreach (Player.Roles role in Enum.GetValues(typeof(Player.Roles)))
            {
                if (left.RoleCondition(role))
                {
                    leftValidRoles += App.Village.GameSetting.RoleCounts[role];
                }
                if (right.RoleCondition(role))
                {
                    rightValidRoles += App.Village.GameSetting.RoleCounts[role];
                }
            }

            return leftValidRoles - rightValidRoles;
        }

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
