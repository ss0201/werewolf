﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WerewolfLibrary;
using WerewolfLibrary.Report;

namespace WerewolfServer
{
    class Bot : Participant
    {
        public Bot(Func<Player.Roles, bool> roleCondition)
            : base()
        {
            RoleCondition = roleCondition;
            Name = "Bot";
        }

        public override void SendReport(Report report)
        {
        }
    }
}
