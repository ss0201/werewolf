﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Net.Sockets;
using System.Net;
using System.Collections.ObjectModel;
using WerewolfLibrary;
using System.IO;
using System.Xml;
using System.ComponentModel;

namespace WerewolfServer
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        private ServerStatus serverStatus = new ServerStatus();
        public MainWindow()
        {
            InitializeComponent();
            ClientListBox.ItemsSource = App.Clients;
            ResetGame();
            Menu.DataContext = serverStatus;
        }

        private void StartAccept()
        {
            App.Communicator.StartAccept(int.Parse(PortTextBox.Text));
            serverStatus.IsAccepting = true;
        }

        private void BeginGame()
        {
            if (App.Clients.Where(c => !c.IsReady).Count() > 0)
            {
                MessageBox.Show("Some clients are not ready.");
                return;
            }

            try
            {
                App.Village.BeginGame();
                serverStatus.IsGameStarted = true;
            }
            catch (XmlException e)
            {
                StatusTextBlock.Text = "Error while reading a variant file";
                MessageBox.Show(e.ToString());
            }
            catch
            {
                StatusTextBlock.Text = "Failed to BeginGame";
            }
        }

        private void ResetGame()
        {
            if (App.Village != null)
            {
                App.Village.Dispose();
            }
            foreach (Client client in App.Clients)
            {
                client.IsReady = false;
            }
            App.Village = new ServerVillage(App.Clients);
            this.DataContext = App.Village;
            serverStatus.IsGameStarted = false;
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            App.Communicator.CloseConnections();
        }

        private void StartAcceptMenuItem_Click(object sender, RoutedEventArgs e)
        {
            StartAccept();
        }

        private void DisconnectMenuItem_Click(object sender, RoutedEventArgs e)
        {
            App.Communicator.CloseConnections();
        }

        private void BeginGameMenuItem_Click(object sender, RoutedEventArgs e)
        {
            BeginGame();
        }

        private void ResetGameMenuItem_Click(object sender, RoutedEventArgs e)
        {
            ResetGame();
        }

        private void GameSettingMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (App.Village.GameSetting == null)
            {
                App.Village.GameSetting = new GameSetting();
            }
            new SettingWindow().Show();
        }

        private void KickMenuItem_Click(object sender, RoutedEventArgs e)
        {
            var client = (Client)ClientListBox.SelectedItem;

            if (MessageBox.Show(string.Format("Really Kick {0}?", client.Name), "Confirm Kick", MessageBoxButton.YesNo)
                == MessageBoxResult.Yes)
            {
                App.Communicator.CloseConnection(client);
            }
        }

        private class ServerStatus : INotifyPropertyChanged
        {
            public bool IsAccepting { get { return isAccepting; } set { isAccepting = value; OnPropertyChanged("IsAccepting"); } }
            public bool IsGameStarted { get { return isGameStarted; } set { isGameStarted = value; OnPropertyChanged("IsGameStarted"); } }
            private bool isAccepting = false;
            private bool isGameStarted = false;

            #region INotifyPropertyChanged

            public event PropertyChangedEventHandler PropertyChanged;

            protected virtual void OnPropertyChanged(string propertyName)
            {
                PropertyChangedEventHandler handler = this.PropertyChanged;
                if (handler != null)
                    handler(this, new PropertyChangedEventArgs(propertyName));
            }

            #endregion
        }
    }
}
