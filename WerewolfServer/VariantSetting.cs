﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WerewolfLibrary;
using System.Xml.Linq;
using System.Runtime.Serialization;
using System.Xml;
using System.IO;

namespace WerewolfServer
{
    public class VariantSetting
    {
        public string VariantName { get { return Path.GetFileName(Properties.Settings.Default.VariantDirectory); } }
        public List<string> PlayerNames { get; private set; }
        public Dictionary<Player.Roles, string> RoleNames { get; private set; }

        public VariantSetting()
        {
            PlayerNames = new List<string>();
            var filePaths = Directory.GetFiles(Properties.Settings.Default.VariantDirectory + @"\" + 
                Constant.PlayerImageDirectory).Where(f => Path.GetFileName(f) != "Thumbs.db");
            foreach (string path in filePaths)
            {
                var name = Path.GetFileNameWithoutExtension(path);
                if (name != Constant.AnonymousName)
                {
                    PlayerNames.Add(name);
                }
            }

            string suffix = @"\" + App.VariantFile;
            string variantFile = Properties.Settings.Default.VariantDirectory + suffix;
            if (!File.Exists(variantFile))
            {
                variantFile = Path.GetDirectoryName(Properties.Settings.Default.VariantDirectory) + @"\" + Constant.DefaultVariantName + suffix;
            }
            var xElement = XElement.Load(variantFile);
            RoleNames = new Dictionary<Player.Roles, string>();
            foreach (Player.Roles role in Enum.GetValues(typeof(Player.Roles)))
            {
                XElement roleElement = xElement.Element(App.RolesTag).Element(role.ToString());
                if (roleElement == null)
                {
                    throw new XmlException(role.ToString() + " tag not found");
                }
                RoleNames.Add(role, roleElement.Value);
            }
        }
    }
}
