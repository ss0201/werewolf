﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Net.Sockets;
using System.Xml.Linq;
using System.IO;
using System.Net;
using WerewolfLibrary;
using System.Security;
using WerewolfLibrary.Report;
using System.Windows.Input;
using System.Windows.Data;
using System.ComponentModel;
using System.Windows.Controls;

namespace WerewolfClient
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        public static RoutedCommand MonologueCommand { get { return monologueCommand; } }
        private static RoutedCommand monologueCommand = new RoutedCommand();
        public static RoutedCommand StrongCommand { get { return strongCommand; } }
        private static RoutedCommand strongCommand = new RoutedCommand();

        public MainWindow()
        {
            InitializeComponent();

            MonologueCommand.InputGestures.Add(new KeyGesture(Key.M, ModifierKeys.Control));
            StrongCommand.InputGestures.Add(new KeyGesture(Key.S, ModifierKeys.Control));
            MiscViewer.ClientListBox.ItemsSource = App.ClientInformations;

            Resources.Add("communicator", App.Communicator);
        }

        public void ResetGame()
        {
            App.Village = new ClientVillage()
            {
                Me = new UncertainPlayer(Properties.Settings.Default.UserName)
            };
            PlayerListBox.ItemsSource = App.Village.Players;
            PlayerListBox.UpdateLayout();
            this.DataContext = App.Village;
            MiscViewer.SetFilters();
            App.Communicator.SendClientInformationReport(ImReadyMenuItem.IsChecked = Constant.DefaultReadyStatus);
        }

        private void SendChat(string text)
        {
            if (App.Village == null) { return; }

            Nullable<StatementReport.Types> type;
            if (MonologueCheckBox.IsEnabled == true && MonologueCheckBox.IsChecked == true)
            {
                type = StatementReport.Types.Monologue;
            }
            else
            {
                type = null;
            }

            App.Village.SendChat(text, type, StrongCheckBox.IsChecked == true);
            ChatTextBox.Text = string.Empty;
            ChatTabControl.SelectedIndex = App.Village.Turn;
        }

        private void ConnectToServerMenuItem_Click(object sender, RoutedEventArgs e)
        {
            new ConnectionWindow()
            {
                Connect = () =>
                {
                    App.Communicator.Connect();
                    if (App.Communicator.IsConnected)
                    {
                        ResetGame();
                        App.Communicator.SendRoleSelectionReport();
                    }
                },
                Owner = this,
            }.ShowDialog();
        }

        private void SendChatButton_Click(object sender, RoutedEventArgs e)
        {
            SendChat(ChatTextBox.Text);
        }

        private void ChatTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return && (Keyboard.Modifiers & ModifierKeys.Shift) != ModifierKeys.Shift)
            {
                SendChat(ChatTextBox.Text);
                e.Handled = true;
            }
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            App.Communicator.CloseConnection();
        }

        private void MonologueCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (MonologueCheckBox.IsEnabled == true)
            {
                MonologueCheckBox.IsChecked = !MonologueCheckBox.IsChecked;
            }
        }

        private void StrongCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (StrongCheckBox.IsEnabled == true)
            {
                StrongCheckBox.IsChecked = !StrongCheckBox.IsChecked;
            }
        }

        private void PlayerBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var player = ((PlayerBox)sender).DataContext as Player;
            if (player != null)
            {
                ChatTextBox.Text += player.Name;
            }
        }

        private void SelectRolesMenuItem_Click(object sender, RoutedEventArgs e)
        {
            new RoleSelectionWindow()
            {
                Owner = this,
            }.ShowDialog();
        }

        private void ImReadyMenuItem_Click(object sender, RoutedEventArgs e)
        {
            App.Communicator.SendClientInformationReport(ImReadyMenuItem.IsChecked);
        }
    }
}
