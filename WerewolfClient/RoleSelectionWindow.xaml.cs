﻿using System.Collections.Generic;
using System.Windows;
using WerewolfLibrary;
using WerewolfLibrary.Report;

namespace WerewolfClient
{
    /// <summary>
    /// RoleSelectionWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class RoleSelectionWindow : Window
    {
        private List<RoleBoolPair> wishes;

        public RoleSelectionWindow()
        {
            InitializeComponent();
            wishes = Utility.DeepCopy(App.Wishes);
            RoleListBox.ItemsSource = wishes;
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            App.Wishes = wishes;
            App.Communicator.SendRoleSelectionReport();
            this.Close();
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
