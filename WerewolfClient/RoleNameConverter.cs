﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using WerewolfLibrary;

namespace WerewolfClient
{
    [ValueConversion(typeof(Player.Roles), typeof(string))]
    public class RoleNameConverter : IValueConverter
    {
        public object Convert(object value, System.Type targetType,
            object parameter, System.Globalization.CultureInfo culture)
        {
            if (App.Village != null && App.Village.GameSetting != null && App.Village.GameSetting.RoleNames != null)
            {
                return App.Village.GameSetting.RoleNames[((Player.Roles)value)];
            }
            else
            {
                return value;
            }
        }

        public object ConvertBack(object value, System.Type targetType,
            object parameter, System.Globalization.CultureInfo culture)
        {
            throw new InvalidOperationException();
        }
    }
}
