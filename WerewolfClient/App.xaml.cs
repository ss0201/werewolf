﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using WerewolfLibrary;
using System.Net.Sockets;
using System.Collections.ObjectModel;

namespace WerewolfClient
{
    /// <summary>
    /// App.xaml の相互作用ロジック
    /// </summary>
    public partial class App : Application
    {
        public const string VariantDirectory = "Variant";
        public const string RoleImageDirectory = "Role";
        public const string CommonImageDirectory = "Common";
        public const string SoundDirectory = "Sound";
        public const string SettingDirectory = "Setting";
        public const string WishesFile = "Wishes.xml";

        public static App CurrentApp { get { return (App)(App.Current); } }

        public static Socket Socket { get; set; }
        public static ClientVillage Village { get; set; }
        public static Communicator Communicator { get; private set; }
        public static ObservableCollection<ClientInformation> ClientInformations { get; private set; }
        public static List<RoleBoolPair> Wishes { get; set; }

        public AbilityWindow AbilityWindow
        {
            get { return abilityWindow; }
            set
            {
                var currentWindow = abilityWindow;
                if (currentWindow != null)
                {
                    currentWindow.Close();
                }
                abilityWindow = value;
            }
        }
        private AbilityWindow abilityWindow;

        public void InitializeProperties()
        {
            Village = new ClientVillage();
            ClientInformations = new ObservableCollection<ClientInformation>();
        }

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            InitializeProperties();

            Communicator = new Communicator();

            if (string.IsNullOrEmpty(WerewolfClient.Properties.Settings.Default.UserName))
            {
                WerewolfClient.Properties.Settings.Default.UserName = Constant.AnonymousName;
            }

            Wishes = Utility.ReadObjectFromFile<List<RoleBoolPair>>(SettingDirectory + @"/" + WishesFile);
            if (Wishes == null)
            {
                Wishes = new List<RoleBoolPair>();
                foreach (Player.Roles role in Enum.GetValues(typeof(Player.Roles)))
                {
                    Wishes.Add(new RoleBoolPair(role, true));
                }
            }
        }

        private void Application_Exit(object sender, ExitEventArgs e)
        {
            WerewolfClient.Properties.Settings.Default.Save();
            Utility.WriteObjectToFile(SettingDirectory + @"/" + WishesFile, Wishes);
        }

        private void Application_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            Utility.WriteToEventLog(e.Exception.ToString());
        }
    }
}
