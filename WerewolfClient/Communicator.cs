﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using WerewolfLibrary;
using System.Net.Sockets;
using System.ComponentModel;
using WerewolfLibrary.Report;
using System.Windows;

namespace WerewolfClient
{
    public class Communicator : INotifyPropertyChanged
    {
        public bool IsConnected { get { return isConnected; } private set { isConnected = value; OnPropertyChanged("IsConnected"); } }
        private bool isConnected = false;

        public void Connect()
        {
            if (App.Socket != null && App.Socket.Connected)
            {
                Utility.ShowStatus("Already connected", App.Current);
                return;
            }

            IPAddress ip = Dns.GetHostAddresses(Properties.Settings.Default.IP)[0];
            IPEndPoint endPoint = new IPEndPoint(ip, Properties.Settings.Default.Port);

            try
            {
                App.Socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                App.Socket.Connect(endPoint);

                AsyncState asyncState = new AsyncState(App.Socket);
                App.Socket.BeginReceive(asyncState.DataBuffer, 0, AsyncState.PrefixLength,
                    SocketFlags.None, new AsyncCallback(ReceiveDataCallback), asyncState);
            }
            catch
            {
                Utility.ShowStatus("Connection failed", App.Current);
                App.Socket = null;
                return;
            }

            SendClientInformationReport(((MainWindow)App.Current.MainWindow).ImReadyMenuItem.IsChecked);

            IsConnected = true;
            Utility.ShowStatus("Connected", App.Current);
        }

        private void ReceiveDataCallback(IAsyncResult asyncResult)
        {
            try
            {
                SocketUtility.ReceiveDataCallback(asyncResult, App.Village,
                    ReceiveDataCallback, text => Utility.ShowStatus(text, App.Current));
            }
            catch (ConnectionClosedException)
            {
                Utility.ShowStatus("Connection has been closed", App.Current);
                CloseConnection();
                App.Current.Dispatcher.BeginInvoke(new Action(() =>
                {
                    if (App.Current.MainWindow != null)
                    {
                        Window tmp = App.Current.MainWindow;
                        App.CurrentApp.InitializeProperties();
                        App.Current.MainWindow = new MainWindow();
                        tmp.Close();
                        App.Current.MainWindow.Show();
                    }
                }));
            }
        }

        public void CloseConnection()
        {
            if (App.Socket != null)
            {
                try
                {
                    App.Socket.Shutdown(SocketShutdown.Both);
                }
                catch { }
                App.Socket.Close();
                App.Socket = null;
            }

            IsConnected = false;
        }

        public void SendRoleSelectionReport()
        {
            SocketUtility.SendReport(App.Socket, new RoleSelectionReport()
            {
                SubjectName = Properties.Settings.Default.UserName,
                Wishes = App.Wishes,
            });
        }

        public void SendClientInformationReport(bool isReady)
        {
            SocketUtility.SendReport(App.Socket, new ClientInformationReport()
            {
                ClientInformations = new List<ClientInformation>()
                {
                    new ClientInformation()
                    {
                        Name = Properties.Settings.Default.UserName,
                        IsReady = isReady,
                    }
                },
            });
        }

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
