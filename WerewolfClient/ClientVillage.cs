﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WerewolfLibrary;
using WerewolfLibrary.Report;
using System.Collections.ObjectModel;
using System.Windows;
using System.Net.Sockets;
using System.ComponentModel;
using System.Windows.Controls;
using System.Timers;
using System.Windows.Data;
using System.Media;
using System.IO;

namespace WerewolfClient
{
    public class ClientVillage : Village
    {
        public ObservableCollection<Report> Reports { get; private set; }
        public ObservableCollection<UncertainPlayer> Players { get; private set; }
        public UncertainPlayer Me { get { return me; } set { me = value; OnPropertyChanged("Me"); } }
        public string VariantName { get; private set; }
        public string AbilityResultName { get { return abilityResultName; } set { abilityResultName = value; OnPropertyChanged("AbilityResultName"); } }
        public Nullable<Player.Colors> AbilityResultColor { get { return abilityResultColor; } set { abilityResultColor = value; OnPropertyChanged("AbilityResultColor"); } }
        private UncertainPlayer me = new UncertainPlayer(string.Empty);
        private string abilityResultName = string.Empty;
        private Nullable<Player.Colors> abilityResultColor = null;
        private Dictionary<Phases, SoundPlayer> soundPlayers = new Dictionary<Phases, SoundPlayer>();

        public ClientVillage()
            : base()
        {
            Reports = new ObservableCollection<Report>();
            Players = new ObservableCollection<UncertainPlayer>();
        }

        private void InvokeToDispatcher(Action action)
        {
            App.Current.Dispatcher.BeginInvoke(action);
        }

        protected override void OnPropertyChanged(string propertyName)
        {
            base.OnPropertyChanged(propertyName);

            switch (propertyName)
            {
                case "Turn":
                    InvokeToDispatcher(() =>
                    {
                        var tabControl = ((MainWindow)App.Current.MainWindow).ChatTabControl;
                        if (Turn == 0)
                        {
                            tabControl.Items.Clear();
                        }

                        var view = new ListCollectionView(Reports);
                        int turn = Turn;
                        view.Filter = new Predicate<object>(target =>
                        {
                            var report = target as StatementReport;
                            return report != null &&
                                report.Type == StatementReport.Types.Normal &&
                                report.Turn == turn;
                        });

                        var item = new TabItem()
                        {
                            Header = Turn,
                            Content = new AutoScrollListBox()
                            {
                                ItemsSource = view,
                                ItemTemplate = (DataTemplate)App.Current.FindResource("statementTemplate"),
                                ItemContainerStyle = (Style)App.Current.FindResource("noSelectionStyle"),
                            },
                        };
                        tabControl.Items.Add(item);
                        tabControl.SelectedItem = item;
                    });
                    break;
                case "Phase":
                    if (Properties.Settings.Default.IsSoundEnabled)
                    {
                        soundPlayers[Phase].Play();
                    }
                    break;
            }
        }

        protected override void Execute(Report report, Socket socket)
        {
            Execute((dynamic)report);
            InvokeToDispatcher(() => Reports.Add(report));
        }

        private void Execute(StatementReport report)
        {
        }

        private void Execute(ClientInformationReport report)
        {
            InvokeToDispatcher(() =>
            {
                App.ClientInformations.Clear();
                foreach (var clientInformation in report.ClientInformations)
                {
                    App.ClientInformations.Add(clientInformation);
                }
            });
        }

        private void Execute(BeginGameReport report)
        {
            GameSetting = report.GameSetting;
            VariantName = report.Variant;
            Turn = 1;

            foreach (string playerName in report.PlayerNames)
            {
                UncertainPlayer player;
                if (playerName == report.YourName)
                {
                    Me = new UncertainPlayer(playerName, report.YourRole);
                    player = Me;
                }
                else
                {
                    bool isAlly = report.Allies.Contains(playerName);
                    player = new UncertainPlayer(playerName, (isAlly ? report.YourRole : Player.Roles.Villager),
                        playerName == report.SacrificeName, isAlly);
                }
                InvokeToDispatcher(() => Players.Add(player));
            }

            foreach (Phases phase in (from p in Enum.GetValues(typeof(Phases)).AsQueryable().Cast<Phases>()
                                      where p != Phases.Preparation
                                      select p))
            {
                string prefix = App.VariantDirectory + @"\";
                string suffix = @"\" + App.SoundDirectory + @"\" + phase + ".wav";
                string file = prefix + App.Village.VariantName + suffix;
                if (!File.Exists(file))
                {
                    file = prefix + Constant.DefaultVariantName + suffix;
                }
                soundPlayers.Add(phase, new SoundPlayer(Path.GetFullPath(file)));
                soundPlayers[phase].Load();
            }
        }

        private void Execute(BeginDayReport report)
        {
            BeginPhase(Phases.Day);
            StartClock(GameSetting.Times[TimeConfigurablePhases.Day]);

            CloseAbilityWindow();

            var killedPlayers = from p in Players where report.KilledNames.Contains(p.Name) select p;

            foreach (UncertainPlayer killedPlayer in killedPlayers)
            {
                killedPlayer.Die(KillMethods.EatOrCurse, Turn);
            }

            AbilityResultName = report.TargetName;
            AbilityResultColor = report.AbilityResult;

            InvokeToDispatcher(() => new BeginDayWindow(killedPlayers)
            {
                Owner = App.Current.MainWindow,
            }.Show());
        }

        private void Execute(BeginVoteReport report)
        {
            BeginPhase(Phases.Vote);
            StartClock(GameSetting.Times[(VoteCount == 1 ? TimeConfigurablePhases.Vote : TimeConfigurablePhases.Revote)]);

            OpenAbilityWindow(player => true, Abilities.Vote);
        }

        private void Execute(EndVoteReport report)
        {
            CloseAbilityWindow();
            if (!string.IsNullOrEmpty(report.KilledName))
            {
                Players.First(p => p.Name == report.KilledName).Die(KillMethods.Lynch, Turn);
            }
        }

        private void Execute(BeginNightReport report)
        {
            BeginPhase(Phases.Night);
            StartClock(GameSetting.Times[TimeConfigurablePhases.Night]);

            switch (me.Role)
            {
                case Player.Roles.Werewolf:
                    if (Turn >= 2)
                    {
                        OpenAbilityWindow(player => !player.IsAlly, Abilities.Kill);
                    }
                    break;
                case Player.Roles.Priest:
                    OpenAbilityWindow(player => !player.IsSacrifice, Abilities.See);
                    break;
                case Player.Roles.Knight:
                    if (Turn >= 2)
                    {
                        OpenAbilityWindow(player => true, Abilities.Guard);
                    }
                    break;
            }
        }

        private void Execute(EndGameReport report)
        {
            BeginPhase(Phases.Gameover);

            Player.Camps mySide;
            switch (Me.Role)
            {
                case Player.Roles.Werewolf:
                case Player.Roles.Fanatic:
                    mySide = Player.Camps.Werewolf; break;
                case Player.Roles.Vampire:
                    mySide = Player.Camps.Vampire; break;
                default:
                    mySide = Player.Camps.Villager; break;
            }
            InvokeToDispatcher(() => new GameoverWindow(mySide == report.Winner, report.Winner)
            {
                Owner = App.Current.MainWindow,
            }.Show());
        }

        private void Execute(ResetGameReport report)
        {
            InvokeToDispatcher(() => ((MainWindow)App.Current.MainWindow).ResetGame());
            CloseAbilityWindow();
        }

        private void Execute(SpoilerReport report)
        {
        }

        public void SendChat(string text, Nullable<StatementReport.Types> type, bool isStrong)
        {
            if (App.Socket == null || string.IsNullOrEmpty(text)) { return; }

            if (type == null)
            {
                if (Me.Status == Player.Statuses.Dead && Phase != Phases.Gameover)
                {
                    type = StatementReport.Types.Graveyard;
                }
                else
                {
                    type = StatementReport.Types.Normal;
                }
            }

            SocketUtility.SendReport(App.Socket, new StatementReport()
            {
                Phase = Phase,
                Turn = Turn,
                Statement = text,
                SubjectName = Me.Name,
                IsStrong = isStrong,
                Type = (StatementReport.Types)type,
            });
        }

        private void OpenAbilityWindow(Func<UncertainPlayer, bool> additionalCondition, Abilities ability)
        {
            if (Me.Status == Player.Statuses.Dead) { return; }

            InvokeToDispatcher(() =>
            {
                Action<Player> sendAbilityReport = (player) =>
                    SocketUtility.SendReport(App.Socket, new AbilityReport()
                    {
                        Phase = Phase,
                        Turn = Turn,
                        SubjectName = Me.Name,
                        TargetName = player.Name,
                    });

                App.CurrentApp.AbilityWindow = new AbilityWindow(new ObservableCollection<UncertainPlayer>(
                    from p in Players
                    where p.Status == Player.Statuses.Alive && !p.Equals(Me) && additionalCondition(p)
                    select p),
                    ability,
                    sendAbilityReport)
                    {
                        Owner = App.Current.MainWindow,
                    };
                App.CurrentApp.AbilityWindow.Show();
            });
        }

        private void CloseAbilityWindow()
        {
            InvokeToDispatcher(() => App.CurrentApp.AbilityWindow = null);
        }
    }
}
