﻿using System;
using System.Windows.Controls;
using System.Windows.Data;
using WerewolfLibrary;
using WerewolfLibrary.Report;

namespace WerewolfClient
{
    /// <summary>
    /// MiscViewer.xaml の相互作用ロジック
    /// </summary>
    public partial class MiscViewer : UserControl
    {
        public MiscViewer()
        {
            InitializeComponent();
        }

        public void SetFilters()
        {
            var systemView = new ListCollectionView(App.Village.Reports);
            systemView.Filter = new Predicate<object>(target =>
            {
                var report = target as Report;
                return report != null && !string.IsNullOrEmpty(report.ToSystemMessage());
            });
            SystemMessageListBox.ItemsSource = systemView;

            var monologueView = new ListCollectionView(App.Village.Reports);
            monologueView.Filter = new Predicate<object>(target =>
            {
                var report = target as StatementReport;
                return report != null && report.Type == StatementReport.Types.Monologue;
            });
            MonologueChatListBox.ItemsSource = monologueView;

            var graveyardView = new ListCollectionView(App.Village.Reports);
            graveyardView.Filter = new Predicate<object>(target =>
            {
                var report = target as StatementReport;
                return report != null && report.Type == StatementReport.Types.Graveyard;
            });
            GraveyardChatListBox.ItemsSource = graveyardView;
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!(e.Source is TabControl))
            {
                return;
            }

            var listBox = ((TabItem)e.AddedItems[0]).Content as AutoScrollListBox;
            if (listBox != null && listBox.Items.Count > 0)
            {
                listBox.SelectedIndex = listBox.Items.Count - 1;
                listBox.ScrollIntoView(listBox.SelectedItem);
            }
        }
    }
}
