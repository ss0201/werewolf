﻿using System.Windows;
using System.Collections.ObjectModel;
using WerewolfLibrary;
using System;
using System.Collections.Generic;

namespace WerewolfClient
{
    /// <summary>
    /// AbilityWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class AbilityWindow : Window
    {
        private Action<UncertainPlayer> sendAbilityReport;

        public AbilityWindow()
        {
            InitializeComponent();
        }

        public AbilityWindow(ObservableCollection<UncertainPlayer> players, Abilities ability, Action<UncertainPlayer> sendAbilityReport)
            : this()
        {
            PlayerListBox.ItemsSource = players;
            TitleStackPanel.DataContext = new { Ability = ability };
            this.sendAbilityReport = sendAbilityReport;
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            sendAbilityReport((UncertainPlayer)((FrameworkElement)sender).DataContext);
        }
    }
}
