﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using WerewolfLibrary.Report;

namespace WerewolfClient
{
    [ValueConversion(typeof(Report), typeof(string))]
    public class ReportToSystemMessageConverter : IValueConverter
    {
        public object Convert(object value, System.Type targetType,
            object parameter, System.Globalization.CultureInfo culture)
        {
            return ((Report)value).ToSystemMessage();
        }

        public object ConvertBack(object value, System.Type targetType,
            object parameter, System.Globalization.CultureInfo culture)
        {
            throw new InvalidOperationException();
        }
    }
}
