﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.IO;

namespace WerewolfClient
{
    [ValueConversion(typeof(string), typeof(BitmapSource))]
    internal class NameImageConverter : IValueConverter
    {
        public static string PlayerSubImageDirectory { private get; set; }
        public static string RoleSubImageDirectory { private get; set; }

        public object Convert(object value, System.Type targetType,
            object parameter, System.Globalization.CultureInfo culture)
        {
            string imageDirectory;
            string subImageDirectory;
            switch ((string)parameter)
            {
                case "Player":
                    imageDirectory = App.PlayerImageDirectory;
                    subImageDirectory = PlayerSubImageDirectory;
                    break;
                case "Role":
                    imageDirectory = App.RoleImageDirectory;
                    subImageDirectory = RoleSubImageDirectory;
                    break;
                case "Common":
                    imageDirectory = App.CommonImageDirectory;
                    subImageDirectory = "default";
                    break;
                default:
                    throw new InvalidOperationException();
            }

            try
            {
                string filePath = string.Empty;
                for (int i = 0; i < 2; i++)
                {
                    if (i == 1)
                    {
                        subImageDirectory = "default";
                        if ((string)parameter == "Player")
                        {
                            value = "NoName";
                        }
                    }
                    foreach (string extension in App.ImageExtensions)
                    {
                        filePath = App.ImageDirectory + @"/" + imageDirectory + @"/" +
                        subImageDirectory + @"/" + value.ToString() + "." + extension;
                        if (File.Exists(filePath))
                        {
                            goto Found;
                        }
                    }
                }

            Found:
                if (filePath == string.Empty)
                {
                    return null;
                }

                var imageStreamSource = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read);
                var decoder = BitmapDecoder.Create(imageStreamSource,
                    BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.Default);

                return decoder.Frames[0];
            }
            catch
            {
                return null;
            }
        }

        public object ConvertBack(object value, System.Type targetType,
            object parameter, System.Globalization.CultureInfo culture)
        {
            throw new InvalidOperationException();
        }
    }
}
