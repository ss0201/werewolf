﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WerewolfClient
{
    /// <summary>
    /// ConnectionWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class ConnectionWindow : Window
    {
        public Action Connect { private get; set; }

        public ConnectionWindow()
        {
            InitializeComponent();
        }

        private void ConnectButton_Click(object sender, RoutedEventArgs e)
        {
            Connect();
            this.Close();
        }
    }
}
