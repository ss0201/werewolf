﻿using System.Windows;
using WerewolfLibrary;
using System.Timers;
using System;

namespace WerewolfClient
{
    /// <summary>
    /// GameoverWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class GameoverWindow : Window
    {
        public bool Win { get; private set; }
        public Player.Camps Winner { get; private set; }

        public GameoverWindow()
        {
            InitializeComponent();
        }

        public GameoverWindow(bool win, Player.Camps winner)
            : this()
        {
            Win = win;
            Winner = winner;
            this.DataContext = this;

            var timer = new Timer();
            timer.AutoReset = false;
            timer.Interval = 5000;
            timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);
            timer.Start();
        }

        void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Dispatcher.BeginInvoke(new Action(() => this.Close()));
        }
    }
}
