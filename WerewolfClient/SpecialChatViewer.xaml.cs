﻿using System;
using System.Windows.Controls;
using System.Windows.Data;
using WerewolfLibrary.Report;

namespace WerewolfClient
{
    /// <summary>
    /// SpecialChatViewer.xaml の相互作用ロジック
    /// </summary>
    public partial class SpecialChatViewer : UserControl
    {
        public SpecialChatViewer()
        {
            InitializeComponent();
        }

        public void SetFilters()
        {
            var systemView = new ListCollectionView(App.Village.Reports);
            systemView.Filter = new Predicate<object>(target =>
            {
                var report = target as Report;
                return report != null && !string.IsNullOrEmpty(report.ToSystemMessage());
            });
            SystemMessageListBox.ItemsSource = systemView;

            var monologueView = new ListCollectionView(App.Village.Reports);
            monologueView.Filter = new Predicate<object>(target =>
            {
                var report = target as StatementReport;
                return report != null && report.Type == StatementReport.Types.Monologue;
            });
            MonologueChatListBox.ItemsSource = monologueView;

            var graveyardView = new ListCollectionView(App.Village.Reports);
            graveyardView.Filter = new Predicate<object>(target =>
            {
                var report = target as StatementReport;
                return report != null && report.Type == StatementReport.Types.Graveyard;
            });
            GraveyardChatListBox.ItemsSource = graveyardView;
        }
    }
}
