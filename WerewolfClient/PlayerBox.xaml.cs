﻿using System.Windows.Controls;
using WerewolfLibrary;
using System.Windows.Documents;

namespace WerewolfClient
{
    /// <summary>
    /// PlayerBox.xaml の相互作用ロジック
    /// </summary>
    public partial class PlayerBox : UserControl
    {
        public PlayerBox()
        {
            InitializeComponent();
        }

        private void AdornedControl_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            this.OnMouseDoubleClick(e);
        }

        private void TextBlock_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (e.ClickCount >= 2)
            {
                this.OnMouseDoubleClick(e);
            }
        }
    }
}
