﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WerewolfLibrary;

namespace WerewolfClient
{
    public class UncertainPlayer : Player
    {
        public bool IsAlly { get; private set; }

        public UncertainPlayer(string name, Roles role = Roles.Villager, bool isSacrifice = false, bool isAlly = false)
            : base(name, role, isSacrifice)
        {
            IsAlly = isAlly;
        }
    }
}
