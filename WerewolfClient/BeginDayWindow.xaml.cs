﻿using System;
using System.Collections.Generic;
using System.Windows;
using WerewolfLibrary;
using System.Timers;

namespace WerewolfClient
{
    /// <summary>
    /// BeginDayWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class BeginDayWindow : Window
    {
        public BeginDayWindow()
        {
            InitializeComponent();
        }

        public BeginDayWindow(IEnumerable<Player> killedPlayers)
            : this()
        {
            PlayerListBox.ItemsSource = killedPlayers;

            var timer = new Timer();
            timer.AutoReset = false;
            timer.Interval = 5000;
            timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);
            timer.Start();
        }

        void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Dispatcher.BeginInvoke(new Action(() => this.Close()));
        }
    }
}
