﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.IO;
using WerewolfLibrary;

namespace WerewolfClient
{
    [ValueConversion(typeof(string), typeof(BitmapSource))]
    public class NameToImageConverter : IValueConverter
    {
        public object Convert(object value, System.Type targetType,
            object parameter, System.Globalization.CultureInfo culture)
        {
            if (value == null) { return null; }
            string imageDirectory;
            switch ((string)parameter)
            {
                case "Player":
                    imageDirectory = Constant.PlayerImageDirectory;
                    break;
                case "Role":
                    imageDirectory = App.RoleImageDirectory;
                    break;
                case "Common":
                    imageDirectory = App.CommonImageDirectory;
                    break;
                default:
                    throw new InvalidOperationException();
            }

            try
            {
                string prefix = App.VariantDirectory + @"\";
                string infix = @"\" + imageDirectory;
                string directory = prefix + App.Village.VariantName + infix;

                string targetFile = string.Empty;
                if (Directory.Exists(directory))
                {
                    string[] files = Directory.GetFiles(directory);
                    foreach (string file in files)
                    {
                        if (value.ToString() == Path.GetFileNameWithoutExtension(file))
                        {
                            targetFile = file;
                        }
                    }
                }
                if (targetFile == string.Empty)
                {
                    targetFile = prefix + Constant.DefaultVariantName + infix + @"\" +
                        ((string)parameter == "Player" ? Constant.AnonymousName : value) + ".png";
                }

                var imageStreamSource = new FileStream(targetFile, FileMode.Open, FileAccess.Read, FileShare.Read);
                var decoder = BitmapDecoder.Create(imageStreamSource,
                    BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.Default);

                return decoder.Frames[0];
            }
            catch
            {
                return null;
            }
        }

        public object ConvertBack(object value, System.Type targetType,
            object parameter, System.Globalization.CultureInfo culture)
        {
            throw new InvalidOperationException();
        }
    }
}
